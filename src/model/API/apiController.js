import axios from 'axios';
const API_URL = 'http://localhost:3000';


export default class APIService {
    constructor() {}
    getAll() {
        return axios.get(API_URL + '/products')
    }
    getId(id) {
        return axios.get(API_URL + '/products/' + id)
    }
    delProd(id) {
        return axios.delete(API_URL + '/products/' + id)
    }
    addProd(newProd) {
        return axios.post(API_URL + '/products', newProd)
    }
    editProd(prod) {
        return axios.put(API_URL + '/products/' + prod.id, prod)
    }


    // categories
    getAllCategories() {
        return axios.get(API_URL + '/categories')
    }
    getIdCategory(id) {
        return axios.get(API_URL + '/categories/' + id)
    }
    delCategory(id) {
        return axios.delete(API_URL + '/categories/' + id)
    }
    addCategory(category) {
        return axios.post(API_URL + '/categories', category)
    }
    editCategory(category) {
        return axios.put(API_URL + '/categories/' + category.id, category)
    }

}
