import Vue from 'vue';
import Vuex from 'vuex';
import Store from '../model/store.class';

Vue.use(Vuex);
import APIService from './../model/API/apiController';
const Api = new APIService();

const store = new Vuex.Store({
    state: {
        almacen: new Store(1),
        categories: [],
    },

    mutations: {
        insertarCategorias(state, category) {
            if (!state.categories.find((cat) => cat.id == category.id)) {
                state.categories.push(category);
            }
        },
        insertarDatos(state, producto) {
            state.almacen.addProductWCategory(
                producto.id,
                producto.name,
                producto.price,
                producto.units,
                producto.category
            );
        },
        deleteCategory(state, category) {
            const id = category.id;
            state.categories = state.categories.filter((category) => category.id != id);

        },
        editCategory(state, category) {
            const posicionCat = state.categories.findIndex(cat => cat.id === category.data.id);

            state.categories.splice(posicionCat, 1, category.data);
        },
        incrementOne(state, id) {
            state.almacen.changeProductUnits(id, +1);
        },
        decrementOne(state, id) {
            state.almacen.changeProductUnits(id, -1);
        },
        deleteProd(state, id) {
            state.almacen.delProduct(id);
        },
        newProd(state, producto) {
            state.almacen.addProductWCategory(
                producto.data.id,
                producto.data.name,
                producto.data.price,
                0,
                producto.data.category
            );
        },
        editProd(state, producto) {
            state.almacen.replaceProduct(
                producto.data.id,
                producto.data.name,
                producto.data.price,
                producto.data.units,
                producto.data.category
            );
        },
    },
    getters: {
        calcularImporte: state => {
            return state.almacen.totalImport();
        },
    },
    actions: {
        deleteAllCategoriesOneConfirm(context, categories) {
            if (confirm(`¿Está seguro de que quiere eliminar ${categories.length} categorías?`, )) {
                categories.forEach(category => {
                    Api.delCategory(category.id).then(() => {
                        context.commit('deleteCategory', category);
                    }).catch(error => {
                        alert(error);
                    });
                });
            }
        },
        getCategories(context) {
            Api.getAllCategories().then(categories => {
                categories.data.forEach(category => {
                    context.commit('insertarCategorias', category);
                });
            }).catch(error => {
                alert(error);
            });
        },
        newCategory(context, category) {
            Api.addCategory(category).then(categoryReceived => {
                context.commit('insertarCategorias', categoryReceived.data);
            }).catch(error => {
                alert(error);
            });
        },
        deleteCategory(context, category) {
            if (confirm(`¿Está seguro de que quiere eliminar ${category.name}?`, )) {
                Api.delCategory(category.id).then(() => {
                    context.commit('deleteCategory', category);
                }).catch(error => {
                    alert(error);
                });
            }
        },
        editCategory(context, category) {
            Api.editCategory(category).then(categoryReceived => {
                context.commit('editCategory', categoryReceived);
            }).catch(error => {
                alert(error);
            });
        },

        deleteAllCategories(context) {
            Api.getAllCategories().then(categories => {
                categories.data.forEach(category => {
                    context.commit('deleteAllCategories', category);
                });
            }).catch(error => {
                alert(error);
            });
        },
        getAll(context) {
            Api.getAll().then(productos => {
                productos.data.forEach(producto => {
                    context.commit('insertarDatos', producto);
                });
            }).catch(error => {
                alert(error);
            });
        },
        incrementOneUnit(context, id) {
            const producto = context.state.almacen.findProduct(id);
            context.commit('incrementOne', producto.id);

            Api.editProd(producto).catch(() => {
                context.commit('decrementOne', producto.id);
            });
        },
        decrementOneUnit(context, id) {
            const producto = context.state.almacen.findProduct(id);
            context.commit('decrementOne', producto.id);
            Api.editProd(producto).catch(() => {
                context.commit('incrementOne', producto.id);
            });
        },
        deleteProd(context, producto) {
            if (confirm(`¿Está seguro de que quiere eliminar el producto: ${producto.name}?`, )) {
                if (producto.units > 0) {
                    if (confirm(`Se eliminará el producto ${producto.name} el cual tiene ${producto.units} unidades.`)) {
                        Api.delProd(producto.id).then(() => {
                            context.commit('deleteProd', producto.id);
                        }).catch(error => {
                            alert(error);
                        });
                    }
                } else {
                    Api.delProd(producto.id).then(() => {
                        context.commit('deleteProd', producto.id);
                    }).catch(error => {
                        alert(error);
                    });
                }
            }
        },
        deleteAllProdsOneConfirm(context, productos) {
            if (confirm(`¿Está seguro de que quiere eliminar ${productos.length} productos?`, )) {
                productos.forEach(producto => {
                    Api.delProd(producto.id).then(() => {
                        context.commit('deleteProd', producto.id);
                    }).catch(error => {
                        alert(error);
                    });
                });
            }

        },
        newProd(context, producto) {
            // añade un producto a la tabla.
            Api.addProd(producto).then(productReceived => {
                context.commit('newProd', productReceived);
            }).catch(error => {
                alert(error);
            });
        },
        editProd(context, prod) {
            Api.editProd(prod).then(productReceived => {
                context.commit('editProd', productReceived);
            }).catch(error => {
                alert(error);
            });
        },
    },
});

export default store;
